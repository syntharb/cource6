﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace cource6.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                name: "BPs",
                columns: table => new
                {
                    subjectId = table.Column<string>(nullable: false),
                    subjectName = table.Column<string>(nullable: true),
                    subjectRole = table.Column<string>(nullable: true),
                    subjectType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BPs", x => x.subjectId);
                });

            migrationBuilder.CreateTable(
                name: "SDs",
                columns: table => new
                {
                    bu = table.Column<string>(nullable: false),
                    plant = table.Column<string>(nullable: false),
                    substr = table.Column<string>(nullable: false),
                    regNo = table.Column<string>(nullable: false),
                    businessPartnerId = table.Column<string>(nullable: true),
                    endDate = table.Column<DateTime>(nullable: false),
                    startDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SDs", x => new { x.bu, x.plant, x.substr, x.regNo });
                    table.ForeignKey(
                        name: "FK_SDs_BPs_businessPartnerId",
                        column: x => x.businessPartnerId,
                        principalTable: "BPs",
                        principalColumn: "subjectId",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "TMs",
                columns: table => new
                {
                    objectId = table.Column<string>(nullable: false),
                    askue = table.Column<string>(nullable: true),
                    averageV = table.Column<float>(nullable: false),
                    cost = table.Column<float>(nullable: false),
                    f = table.Column<string>(nullable: true),
                    kn = table.Column<string>(nullable: true),
                    model = table.Column<string>(nullable: true),
                    no = table.Column<string>(nullable: true),
                    power = table.Column<float>(nullable: false),
                    supplyDocumentbu = table.Column<string>(nullable: true),
                    supplyDocumentplant = table.Column<string>(nullable: true),
                    supplyDocumentregNo = table.Column<string>(nullable: true),
                    supplyDocumentsubstr = table.Column<string>(nullable: true),
                    tm = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMs", x => x.objectId);
                    table.ForeignKey(
                        name: "FK_TMs_SDs_supplyDocumentbu_supplyDocumentplant_supplyDocumentsubstr_supplyDocumentregNo",
                        columns: x => new { x.supplyDocumentbu, x.supplyDocumentplant, x.supplyDocumentsubstr, x.supplyDocumentregNo },
                        principalTable: "SDs",
                        principalColumns: new[] { "bu", "plant", "substr", "regNo" },
                        onDelete: ReferentialAction.SetNull);
                });

       

            migrationBuilder.CreateIndex(
                name: "IX_SDs_businessPartnerId",
                table: "SDs",
                column: "businessPartnerId");

            migrationBuilder.CreateIndex(
                name: "IX_TMs_supplyDocumentbu_supplyDocumentplant_supplyDocumentsubstr_supplyDocumentregNo",
                table: "TMs",
                columns: new[] { "supplyDocumentbu", "supplyDocumentplant", "supplyDocumentsubstr", "supplyDocumentregNo" });


        }


    }
}
