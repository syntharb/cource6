﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cource6.Models.DataModels
{
    public class BusinessPartner
    {
        public string subjectId { get; set; }
        public string subjectType { get; set; }
        public string subjectRole { get; set; }
        public string subjectName { get; set; }
        public List<SupplyDocument> supplyDocuments { get; set; }
    }

}
