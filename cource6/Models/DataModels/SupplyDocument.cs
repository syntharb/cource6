﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cource6.Models.DataModels
{
    public class SupplyDocument
    {
        public string bu { get; set; }
        public string plant { get; set; }
        public string substr { get; set; }
        public string regNo { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string businessPartnerId { get; set; }
        public BusinessPartner businessPartner { get; set; }
        public List<TM> tms { get; set; }
    }
}
