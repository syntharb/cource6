﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cource6.Models.DataModels
{
    public class TM
    {
        public string objectId { get; set; }
        public string nextTm { get; set; }
        public float averageV { get; set; }
        public float cost { get; set; }
        public string kn { get; set; }
        public float power { get; set; }
        public string f { get; set; }
        public string askue { get; set; }
        public string model { get; set; }
        public string no { get; set; }
        public SupplyDocument supplyDocument { get; set; }
    }
}
