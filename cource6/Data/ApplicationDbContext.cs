﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using cource6.Models;
using cource6.Models.DataModels;

namespace cource6.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public DbSet<BusinessPartner> BPs { get; set; }
        public DbSet<SupplyDocument> SDs { get; set; }
        public DbSet<TM> TMs { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<BusinessPartner>().HasKey(b => new { b.subjectId });
            builder.Entity<SupplyDocument>().HasKey(s => new { s.bu, s.plant, s.substr, s.regNo });
            builder.Entity<SupplyDocument>().HasOne(s => s.businessPartner).WithMany(b => b.supplyDocuments).OnDelete(DeleteBehavior.SetNull);
            builder.Entity<TM>().HasKey(t => new { t.objectId });
            builder.Entity<TM>().HasOne(t => t.supplyDocument).WithMany(s => s.tms).OnDelete(DeleteBehavior.SetNull);
        }
    }
}
